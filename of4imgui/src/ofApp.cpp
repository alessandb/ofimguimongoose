﻿#include "ofApp.h"

#include "mongoose.h"
#include <string>
#include <sstream>
#include <vector>

void split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}

std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}


static const char *s_http_port = "8000";
static struct mg_serve_http_opts s_http_server_opts;

static void ev_handler(struct mg_connection *c, int ev, void *p) {
	if (ev == MG_EV_HTTP_REQUEST) {
		struct http_message *hm = (struct http_message *) p;

		
		std::string uri("nil");
		uri.assign(hm->uri.p, hm->uri.len);
		cout << uri << endl;
		std::vector<std::string> els = split(uri, '/');
		//cout << els.at(1) << endl;
		cout << els.size() << endl;

		// We have received an HTTP request. Parsed request is contained in `hm`.
		// Send HTTP reply to the client which shows full original request.
		mg_send_head(c, 200, hm->message.len, "Content-Type: text/plain");
		//mg_printf(c, "%.*s", hm->message.len, hm->message.p);
		mg_printf(c, "%.*s", hm->uri.len, hm->uri);
		mg_printf(c, "%.*s", hm->message.len, hm->message.p);


	}
}

struct mg_mgr mgr;
struct mg_connection *nc;

int64_t ctr = 0;


//--------------------------------------------------------------

void ofApp::setup()
{

	ofSetLogLevel(OF_LOG_VERBOSE);

	//required call
	gui.setup();

	ImGui::GetIO().MouseDrawCursor = false;
	//backgroundColor is stored as an ImVec4 type but can handle ofColor
	backgroundColor = ofColor(88, 88, 88);
	show_test_window = true;
	show_another_window = false;
	floatValue = 0.0f;

	//load your own ofImage
	imageButtonSource.load("‪\\Users\\ab\\Desktop\\JENNIFER.jpg");
	imageButtonID = gui.loadImage(imageButtonSource);

	//or have the loading done for you if you don't need the ofImage reference
	//imageButtonID = gui.loadImage("of.png");

	//can also use ofPixels in same manner
	ofLoadImage(pixelsButtonSource, "‪\\Users\\ab\\Desktop\\logo_sito_link.jpg");
	pixelsButtonID = gui.loadPixels(pixelsButtonSource);

	//and alt method
	//pixelsButtonID = gui.loadPixels("of_upside_down.png");

	//pass in your own texture reference if you want to keep it
	textureSourceID = gui.loadTexture(textureSource, "of_upside_down.png");

	//or just pass a path
	//textureSourceID = gui.loadTexture("of_upside_down.png");

	ofLogVerbose() << "textureSourceID: " << textureSourceID;
	ofLog(OF_LOG_NOTICE, "Starting.");
	//ofHttpResponse resp = ofLoadURL("http://www.google.com/robots.txt");
	//cout << resp.data << endl;



	mg_mgr_init(&mgr, NULL);
	printf("Starting web server on port %s\n", s_http_port);
	nc = mg_bind(&mgr, s_http_port, ev_handler);
	if (nc == NULL) {
		printf("Failed to create listener\n");
	}

	// Set up HTTP server parameters, useless now?
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = ".";  // Serve current directory
	s_http_server_opts.enable_directory_listing = "yes";


}

bool doSetTheme = false;
//--------------------------------------------------------------
void ofApp::update() {

	if (doSetTheme)
	{
		doSetTheme = false;
		gui.setTheme(new ThemeTest());

	}

	ctr++;

	if ((ctr % 100) == 1) {
	mg_mgr_poll(&mgr, 10);
	//ofDrawBezier(0.0f, 0.0f, 1.0f, (float) ctr, 100.0f, 10.0f, 200.0f, 300.0f);
	}
}
bool doThemeColorsWindow = false;
//--------------------------------------------------------------
void ofApp::draw() {
	ctr++;
	ofDrawBezier(0.0f, (float)(1999 - ctr), (float)(ctr%999), (float)ctr, 500.0f, 500.0f, (float)(ctr % 499), (float) (999-ctr));
	//backgroundColor is stored as an ImVec4 type but is converted to ofColor automatically

	//ofSetBackgroundColor(backgroundColor);

	//required to call this at beginning
	gui.begin();

	//In between gui.begin() and gui.end() you can use ImGui as you would anywhere else

	// 1. Show a simple window
	{
		ImGui::Text("Hello, world!");
		ImGui::SliderFloat("Float", &floatValue, 0.0f, 1.0f);

		//this will change the app background color
		ImGui::ColorEdit3("Background Color", (float*)&backgroundColor);
		if (ImGui::Button("Test Window"))
		{
			show_test_window = !show_test_window;
		}

		if (ImGui::Button("Another Window"))
		{
			//bitwise OR
			show_another_window ^= 1;

		}
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}
	// 2. Show another window, this time using an explicit ImGui::Begin and ImGui::End
	if (show_another_window)
	{
		//note: ofVec2f and ImVec2f are interchangeable
		ImGui::SetNextWindowSize(ofVec2f(200, 100), ImGuiSetCond_FirstUseEver);
		ImGui::Begin("Another Window", &show_another_window);
		ImGui::Text("Hello, daje");
		//ofLog(OF_LOG_NOTICE, "nuwin the number is %d", 10);
		ImGui::End();
	}

	// 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	if (show_test_window)
	{
		ImGui::SetNextWindowPos(ofVec2f(650, 20), ImGuiSetCond_FirstUseEver);
		ImGui::ShowTestWindow(&show_test_window);
	}


	bool pressed = ImGui::ImageButton((ImTextureID)(uintptr_t)imageButtonID, ImVec2(200, 200));
	pressed = ImGui::ImageButton((ImTextureID)(uintptr_t)pixelsButtonID, ImVec2(200, 200));
	pressed = ImGui::ImageButton((ImTextureID)(uintptr_t)textureSourceID, ImVec2(200, 200));


	if (doThemeColorsWindow)
	{
		gui.openThemeColorWindow();

	}

	//required to call this at end
	gui.end();

	if (textureSource.isAllocated())
	{
		//textureSource.draw(ofRandom(200), ofRandom(200));
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

	ofLogVerbose(__FUNCTION__) << key;
	switch (key)
	{
	case 't':
	{
		doThemeColorsWindow = !doThemeColorsWindow;
		break;
	}
	case 'c':
	{
		doSetTheme = !doSetTheme;
		break;
	}
	case 's':
	{
		break;
	}
	}

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	ofLogVerbose(__FUNCTION__) << key;

}


void ofApp::mouseScrolled(float x, float y)
{
	ofLogVerbose(__FUNCTION__) << "x: " << x << " y: " << y;
}
//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}